import { Injectable } from '@angular/core';
import {
  Router,
  CanActivate,
  ActivatedRouteSnapshot,
  RouterStateSnapshot
} from '@angular/router';

import { AuthenticationService } from '../services/_index';

/**
 * The auth guard is an angular route guard that's used to prevent
 * unauthenticated users from accessing restricted routes.
 * It does this by implementing the @method CanActivate interface which allows
 * the guard to decide if a route can be activated with the
 * @method canActivate() method.
 */

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  constructor(
    private router: Router,
    private authenticationService: AuthenticationService
  ) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    const currentUser = this.authenticationService.currentUserValue;
    if (currentUser) {
      return true;
    }

    this.router.navigate(
      ['/login'],
      { queryParams: { returnUrl: state.url } }
    );
    return false;
  }
}
