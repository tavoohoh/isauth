import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { ToastrService } from 'ngx-toastr';

/**
 * Service that hosts methods of common uses within the application.
 * Host the @method onNavigate()
 * and @method errorHandler()
 */

@Injectable()
export class CommonsService {

  constructor(
    private translate: TranslateService,
    private router: Router,
    private toastr: ToastrService
  ) { }

  /**
   * Navigation helper
   */
  onNavigate(route: string) {
    this.router.navigate([route]);
  }

  /**
   * Errors handler
   */
  errorHandler(e: any) {

    switch (e) {
      case 'Username was not found':
        return this.errorAlert(
          this.translate.get('error.404')['value'],
          this.translate.get('error.no_user')['value']
        );

      case 'Username or password is incorrect':
        return this.errorAlert(
          this.translate.get('error.404')['value'],
          this.translate.get('error.invalid_credentials')['value']
        );

      case 'Username already in use':
        return this.errorAlert(
          this.translate.get('error.409')['value'],
          this.translate.get('error.user_exist')['value']
        );

      default:
        return this.errorAlert(
          this.translate.get('error.0')['value'],
          this.translate.get('error.0_TXT')['value']
        );
    }
  }

  /**
   * Generic alert
   */
  errorAlert(title: string, text: string) {
    this.toastr.error(text, title);
  }

}

