import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

/**
 * This is a small component that allows the user to change the
 * language from English to Spanish or vice versa.
 * It also sets the default language of the session.
 */

@Component({
  selector: 'app-language',
  template: `
    <button class="btn_lang" (click)="changeLanguage($event)">{{ 'global.lang' | translate }}</button>
  `,
  styleUrls: ['./language.component.scss']
})
export class LanguageComponent implements OnInit {

  constructor(private translate: TranslateService) { }

  ngOnInit() {
    console.log('lang init');
    if (localStorage.getItem('lang')) {
      this.translate.use(localStorage.getItem('lang'));
    }
  }

  changeLanguage(event: Event) {
    console.log('change lang');
    const target = event.target || event.srcElement || event.currentTarget;
    const lang = target['innerHTML'];
    localStorage.setItem('lang', lang);
    this.translate.use(lang);
  }

}
