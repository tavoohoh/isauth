import {
  Component,
  HostListener,
  Output,
  EventEmitter
} from '@angular/core';

/**
 * Allows showing a modal that receives and delivers parameters
 */

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.scss']
})
export class ModalComponent {

  @Output() modalResult = new EventEmitter<boolean>();

  modalDisplay = false;
  animate = false;
  action = true;
  animationTimeOut = 100;
  modal = {};

  @HostListener('document:keydown.escape', ['$event'])
  onKeydownHandler(event: KeyboardEvent) {
    if (event.key === 'Escape') {
      this.dismiss();
    }
  }

  constructor() { }

  dismiss() {
    this.animate = false;

    setTimeout(() => {
      this.modalDisplay = false;
    }, this.animationTimeOut);
  }

  accept() {
    this.dismiss();
    this.modalResult.emit(this.action);
  }

  open(modal: {}, noAction: boolean) {
    this.modal = modal;
    this.modalDisplay = true;
    this.action = noAction;

    setTimeout(() => {
      this.animate = true;
    }, this.animationTimeOut);
  }

}
