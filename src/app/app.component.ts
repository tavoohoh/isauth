import { Component } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { AuthenticationService } from './services/_index';
import { User } from './_models/users';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  currentUser: User;

  constructor(
    private translate: TranslateService,
    private authenticationService: AuthenticationService
  ) {

    // set language to english by default
    translate.setDefaultLang('en');

    this.authenticationService.currentUser.subscribe(x => this.currentUser = x);
  }






}
