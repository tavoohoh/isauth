import { environment } from '../environments/environment';

const server = environment.apiUrl;

export const api = {

  /**
   * AUTH: Register a new user
   * HTTP @method POST
   *
   * Params
   *   {
   *     "username": "string",
   *     "password": "string"
   *   }
   *
   */
  AUTH_REGISTER: server + 'auth/register',

  /**
   * AUTH: Log-in a user
   * HTTP @method POST
   *
   * Params
   *   {
   *     "username": "string",
   *     "password": "string"
   *   }
   *
   */
  AUTH_LOGIN: server + 'auth/login',

  /**
   * List users
   * HTTP @method GET
   *
   * Response
   *   [
   *     {
   *       "_id": "string",
   *       "username": "string"
   *      }
   *   ]
   *
   */
  USERS_LIST: server + 'users',

  /**
   * Delete a user
   * HTTP @method DELETE
   */
  USERS_DELETE: server + 'users/{id}',

};
