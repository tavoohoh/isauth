import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { first } from 'rxjs/operators';

import { AuthenticationService } from '../../../services/_index';
import { CommonsService } from '../../../_helpers/_index';

/**
 * Host the LOGIN page.
 * It allows the user to log in to the application
 * using the @method login() defined in "services/authentication.service.ts".
 */

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  submitted = false;
  returnUrl: string;

  constructor(
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private authenticationService: AuthenticationService,
    private commonsService: CommonsService
  ) { }

  ngOnInit() {
    this.setLoginForm();

    // reset login status
    this.authenticationService.logout();

    // get return url from route parameters or default to '/'
    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
  }

  /**
   * Initialize login form with all its fields
   * email and password
   */
  setLoginForm() {
    this.loginForm = this.formBuilder.group({
      email: ['', [
        Validators.required,
        Validators.email,
        Validators.pattern('^[a-zA-Z0-9._%+-]+@([a-z0-9.-])+\.[a-z]{2,5}$')
      ]],
      password: ['', [Validators.required, Validators.minLength(6)]]
    });
  }

  /**
   * Login form submit
   * Send the form to the server and log in the user
   */
  onSubmit() {
    this.submitted = true;

    // stop here if form is invalid
    if (this.loginForm.invalid) {
      return;
    }

    // And it is sent to the api service
    this.authenticationService.login(this.f.email.value, this.f.password.value)
      .pipe(first())
      .subscribe(
        data => {
          this.router.navigate([this.returnUrl]);
        },
        error => {
          this.commonsService.errorHandler(error);
        });
  }

  /**
   * Convenience getter for easy access to form fields
   */
  get f() {
    return this.loginForm.controls;
  }

  /**
   * Navigation helper
   */
  onNavigate(route: string) {
    return this.commonsService.onNavigate(route);
  }
}
