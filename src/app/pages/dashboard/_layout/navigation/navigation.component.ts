import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { AuthenticationService } from '../../../../services/_index';

/**
 * It is a small component that shows a sidebar (on large screens)
 * or a navigation bar (on small screens). It is an indispensable
 * component for the basic layout of pages such as USERS.
 */

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.scss']
})
export class NavigationComponent {
  public today = new Date();
  public user = JSON.parse(localStorage.getItem('currentUser')).username;

  constructor(
    private router: Router,
    private authenticationService: AuthenticationService
  ) {}

  logout() {
    this.authenticationService.logout();
    this.router.navigate(['/login']);
  }

}
