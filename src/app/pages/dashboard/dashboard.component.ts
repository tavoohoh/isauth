import { Component } from '@angular/core';

/**
 * It hosts a layout composed of the "footer" and "navigation" component.
 * It serves as a basis for displaying the pages declared by
 * @module DasboardModule in "pages/dashboard/dashboard.module.ts".
 */

@Component({
  selector: 'app-dashboard',
  template: `
    <app-navigation></app-navigation>
    <router-outlet></router-outlet>
    <app-footer></app-footer>
  `
})
export class DashboardComponent {

  constructor() { }

}
