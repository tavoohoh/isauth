import { Component, OnInit, ViewChild } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { first } from 'rxjs/operators';

import { User } from '../../../_models/users';
import { UsersService } from 'src/app/services/_index';
import { ModalComponent } from '../../../_shared/modal/modal.component';

/**
 * Host the USERS page.
 * List registered users in the system.
 * The list has pagination and a filter.
 * Pagination is implemented thanks to the "ngx-pagination" package
 * Filter using the pipe hosted in "_pipes/pipe.ts".
 */

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit {
  public filterValue: string;
  public users: User[] = [];
  public modalData: {};
  public currentPage: any;

  @ViewChild(ModalComponent) modalComponent;

  constructor(
    private userService: UsersService,
    private translate: TranslateService
  ) { }

  ngOnInit() {
    this.getAllUsers();
  }

  /**
   * Get all user form the server
   */
  getAllUsers() {
    this.userService.listUsers().pipe(first()).subscribe(users => {
      this.users = users;
    });
  }

  /**
   * eliminates a user
   * @param user_id
   * The user that will be deleted
   */
  deleteUser(user_id: string) {
    this.userService.deleteUser(user_id).pipe(first()).subscribe(data => {
      this.getAllUsers();
    });
  }

  /**
   * Display a modal confirmation
   * @param user
   * The user that will be deleted
   */
  openDeleteModal(user: any) {
    // save the user data for later use
    this.modalData = user;

    if (user.username === JSON.parse(localStorage.getItem('currentUser')).username) {
      this.modalComponent.open({
        title: this.translate.get('info.delete_user')['value'],
        text: this.translate.get('error.delete_user_txt')['value'],
        confirm: this.translate.get('action.sorry')['value'],
      });
    } else {
      let text = this.translate.get('info.delete_user_txt')['value'];
      text = text.replace('{{ user }}', user.username);

      this.modalComponent.open({
        title: this.translate.get('info.delete_user')['value'],
        text: text,
        confirm: this.translate.get('action.delete')['value'],
        dismiss: this.translate.get('action.cancel')['value'],
      }, true);
    }
  }

  /**
   * On modal dismiss
   */
  dismissDeleteModal($event) {
    if ($event) {
      this.deleteUser(this.modalData['_id']);
    }
  }

}
