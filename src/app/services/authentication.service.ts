import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { api } from '../globals';
import { User } from '../_models/users';

/**
 * It hosts the methods and logic of the services
 * @method login()
 * @method signup()
 * @method logout()
 */

@Injectable({ providedIn: 'root' })
export class AuthenticationService {
  private currentUserSubject: BehaviorSubject<User>;
  public currentUser: Observable<User>;

  /* SERVER API URLs */
  url_login = api.AUTH_LOGIN;
  url_register = api.AUTH_REGISTER;

  /**
   * Base request options
   * Header will be added later by the
   * AuthGuard guard in "_guards/auth.guard.ts"
   */
  reqOpts = {
    params: new HttpParams()
  };

  constructor(private http: HttpClient) {
    this.currentUserSubject = new BehaviorSubject<User>(
      JSON.parse(localStorage.getItem('currentUser'))
    );
    this.currentUser = this.currentUserSubject.asObservable();
  }

  /**
   * Check for the user status
   */
  public get currentUserValue(): User {
    return this.currentUserSubject.value;
  }

  /**
   * Log in service
   * @param username users email
   * @param password users password
   * After a successful log in it save the user in the localStorage
   */
  login(username: string, password: string) {

    return this.http.post<any>(this.url_login, { username, password }, this.reqOpts)
      .pipe(map(user => {
        // login successful if there's a jwt token in the response
        if (user && user.accessToken) {
          // store user token in local storage to keep user logged in between page refreshes
          localStorage.setItem('currentUser', JSON.stringify(user));
          this.currentUserSubject.next(user);
        }

        return user;
      }));
  }

  /**
   * Sign up service
   * @param username users email
   * @param password users password
   */
  signup(username: string, password: string) {

    return this.http.post<any>(this.url_register, { username, password }, this.reqOpts)
      .pipe(map(user => {
        return user;
      }));
  }

  /**
   * remove user currentUser from local storage to log user out
   */
  logout() {
    localStorage.removeItem('currentUser');
    this.currentUserSubject.next(null);
  }
}
