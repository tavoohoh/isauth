import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { map } from 'rxjs/operators';

import { api } from '../globals';
import { User } from '../_models/users';

/**
 * It hosts the methods and logic of the services
 * @method listUsers()
 * @method deleteUser()
 */

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  /* SERVER API URLs */
  url_users_list = api.USERS_LIST;
  url_users_delete = api.USERS_DELETE;

  /**
   * Base request options
   * Header will be added later by the
   * AuthGuard guard in "_guards/auth.guard.ts"
   */
  reqOpts = {
    params: new HttpParams()
  };

  constructor(private http: HttpClient) { }

  /**
   * list users
   */
  listUsers() {
    return this.http.get<User[]>(this.url_users_list, this.reqOpts);
  }

  /**
   * delete user
   */
  deleteUser(user_id: string) {
    const url = this.url_users_delete.replace('{id}', user_id);

    return this.http.delete<any>(url, this.reqOpts)
      .pipe(map(result => {
        return result;
      }));
  }

}
